package com.company;

public class Main {

    public static void main(String[] args) {

        int     numero1 = 25;
        double  numero2 = 45.5;
        long    numero3 = 10000000000L;

// Forma 1: Con método toString()
        String cadena1 = Integer.toString(numero1);
        String cadena2 = Double.toString(numero2);

// Forma 2: Con método valueOf() de String
        String cadena3 = String.valueOf(numero3);
    }
}
